package com.example.erang.facerecognition.Tasks;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.erang.facerecognition.Activities.ResultsActivity;
import com.example.erang.facerecognition.ObjectClass.FaceDetails;
import com.example.erang.facerecognition.ObjectClass.Person;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by erang on 13-Jul-17.
 */

public class QueryFaceDetailsTask extends AsyncTask<ArrayList<FaceDetails>,Void,ArrayList<Person>> {
    private Activity activity;
    public QueryFaceDetailsTask ( Activity activity){

        this.activity = activity;

    }

    /*
    * This Array receive the name of the key for each query.
    * We then check the size of it, if it contain 14 keys name then
    * there is a perfect match and we write the person name to the json file.
    * */

    private int time = 0;

    private ArrayList<String> listOfMatches = new ArrayList<>();
    /*
    * This is for multiple images with multiple faces.
    * If the 28 queries find the same person on diffrent image then the code,
    * will not update the json file with new results.
    * We do not want to retrieve multiple IDs for the same person if apears
    * on diffrent images.
    *
    * */
   private ArrayList<String>keyListQueryFaceSizeHeight = new ArrayList<>();
   private ArrayList<String>keyListQueryFaceSizeWidth = new ArrayList<>();
   private ArrayList<String>keyListQueryLeftEyeBrowSizeHeight = new ArrayList<>();
   private ArrayList<String>keyListQueryLeftEyeBrowSizeWidth = new ArrayList<>();
   private ArrayList<String>keyListQueryLeftEyeSizeHeight = new ArrayList<>();
   private ArrayList<String>keyListQueryLeftEyeSizeWidth = new ArrayList<>();
    private ArrayList<String>keyListQueryMouthSizeHeight = new ArrayList<>();
    private ArrayList<String>keyListQueryMouthSizeWidth = new ArrayList<>();
    private ArrayList<String>keyListQueryNoseSizeHeight = new ArrayList<>();
    private ArrayList<String>keyListQueryNoseSizeWidth = new ArrayList<>();
    private ArrayList<String>keyListQueryRightEyeBrowSizeHeight = new ArrayList<>();
    private ArrayList<String>keyListQueryRightEyeBrowSizeWidth = new ArrayList<>();
    private ArrayList<String>keyListQueryRightEyeSizeWidth = new ArrayList<>();
    private ArrayList<String>keyListQueryRightEyeSizeHeight = new ArrayList<>();


    //Connect to DataBase
    final private FirebaseDatabase database = FirebaseDatabase.getInstance();
    //Create ref to People
    private DatabaseReference myRef = database.getReference("People");

    //Create exmpty matchesIndex to count the number of matches

    private int matchesIndex =0;

    @Override
    protected ArrayList<Person> doInBackground(ArrayList<FaceDetails>[] arrayLists) {
        ArrayList<Person> personArrayList = new ArrayList<>();
            ArrayList<FaceDetails> faceDetailsArrayList = arrayLists[0];
            for(FaceDetails faceDetails:faceDetailsArrayList){
                queryEntireFace(faceDetails);
                //waiting for query results coming from the cloud.
                while(time == 0){

                }
                time = 0;
                Person person = retrieveMatchedDataResults();
                if(person.name != null){
                    personArrayList.add(person);

                }
            }



        return personArrayList;
    }


    @Override
    protected void onPostExecute(ArrayList<Person> personArrayList){


        if(personArrayList.size() != 0){
            Intent intent = new Intent(activity, ResultsActivity.class);
            intent.putExtra("personArrayList",personArrayList);
            activity.startActivity(intent);
        }


    }

    private Person retrieveMatchedDataResults(){
        Person person = new Person();



            String results = mReadJsonData();
            if(results != ""){
            Log.d(TAG,"The results are:\n" + results);
            try {
                JSONObject personJson = new JSONObject(results);
                String address = personJson.getString("address");
                int age = personJson.getInt("age");
                String id = personJson.getString("id");
                String image = personJson.getString("image");
                String name = personJson.getString("name");
                Map<String, Object> children = new Gson().fromJson(String.valueOf(personJson.getJSONObject("children")), Map.class);
                Map<String, Object> faceDetails = new Gson().fromJson(String.valueOf(personJson.getJSONObject("faceDetails")), Map.class);
                person = new Person(name,age,id,children,address,image,faceDetails);
                File jsonfile = new File((activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                        + File.separator + "people.json"));
             if(jsonfile.delete()){
                 Log.d(TAG,"File deleted successfully" + jsonfile);
             }
             else{
                 Log.d(TAG,"Unable to delete the file" + jsonfile);
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
            return person;
    }

    @AddTrace(name = "queryEntireFace", enabled = true/*Optional*/)
    private void queryEntireFace(FaceDetails faceDetails){
        queryFaceSizeHeight(faceDetails);
        queryFaceSizeWidth(faceDetails);
        queryLeftEyeBrowSizeHeight(faceDetails);
         queryLeftEyeBrowSizeWidth(faceDetails);
        queryLeftEyeSizeHeight(faceDetails);
        queryLeftEyeSizeWidth(faceDetails);
        queryMouthSizeHeight(faceDetails);
        queryMouthSizeWidth(faceDetails);
        queryNoseSizeHeight(faceDetails);
        queryNoseSizeWidth(faceDetails);
        queryRightEyeBrowSizeHeight(faceDetails);
        queryRightEyeBrowSizeWidth(faceDetails);
        queryRightEyeSizeWidth(faceDetails);
        queryRightEyeSizeHeight(faceDetails);


    }
    @AddTrace(name = "queryFaceSizeHeight", enabled = true/*Optional*/)
    private void queryFaceSizeHeight(FaceDetails faceDetails){

        Query personFaceSizeHeightQueryStartAt = myRef.orderByChild("mCalculateFaceSizeHeight")
                .startAt(faceDetails.getmCalculateFaceSizeHeight())
                .limitToFirst(1);
        personFaceSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personFaceSizeHeightQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryFaceSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeHeightQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeHeightQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeHeightQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personFaceSizeHeightQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personFaceSizeHeightQueryEndAt = myRef.orderByChild("mCalculateFaceSizeHeight")
                .endAt(faceDetails.getmCalculateFaceSizeHeight())
                .limitToLast(1);
        personFaceSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personFaceSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryFaceSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeHeightQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personFaceSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryFaceSizeWidth", enabled = true/*Optional*/)
    private void queryFaceSizeWidth(FaceDetails faceDetails){

        Query personFaceSizeWidthQueryStartAt = myRef.orderByChild("mCalculateFaceSizeWidth")
                .startAt(faceDetails.getmCalculateFaceSizeHeight())
                .limitToFirst(1);
        personFaceSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personFaceSizeWidthQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryFaceSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personFaceSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personFaceSizeWidthQueryEndAt = myRef.orderByChild("mCalculateFaceSizeWidth")
                .endAt(faceDetails.getmCalculateFaceSizeHeight())
                .limitToLast(1);
        personFaceSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personFaceSizeWidthQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryFaceSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeWidthQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeWidthQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personFaceSizeWidthQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personFaceSizeWidthQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryLeftEyeBrowSizeHeight", enabled = true/*Optional*/)

    private void queryLeftEyeBrowSizeHeight(FaceDetails faceDetails){

        Query personLeftEyeBrowSizeHeightQueryStartAt = myRef.orderByChild("mCalculateLeftEyeBrowSizeHeight")
                .startAt(faceDetails.getmCalculateLeftEyeBrowSizeHeight())
                .limitToFirst(1);
        personLeftEyeBrowSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "queryLeftEyeBrowSizeHeight:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeBrowSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryLeftEyeBrowSizeHeight:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryLeftEyeBrowSizeHeight:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryLeftEyeBrowSizeHeight:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "queryLeftEyeBrowSizeHeight:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personLeftEyeBrowSizeHeightQueryEndAt = myRef.orderByChild("mCalculateLeftEyeBrowSizeHeight")
                .endAt(faceDetails.getmCalculateLeftEyeBrowSizeHeight())
                .limitToLast(1);
        personLeftEyeBrowSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeBrowSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeBrowSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeHeightQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeBrowSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryLeftEyeBrowSizeWidth", enabled = true/*Optional*/)

    private void queryLeftEyeBrowSizeWidth(FaceDetails faceDetails){

        Query personLeftEyeBrowSizeWidthQueryStartAt = myRef.orderByChild("mCalculateLeftEyeBrowSizeWidth")
                .startAt(faceDetails.getmCalculateLeftEyeBrowSizeWidth())
                .limitToFirst(1);
        personLeftEyeBrowSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeBrowSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeBrowSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personLeftEyeBrowSizeWidthQueryEndAt = myRef.orderByChild("mCalculateLeftEyeBrowSizeWidth")
                .endAt(faceDetails.getmCalculateLeftEyeBrowSizeWidth())
                .limitToLast(1);
        personLeftEyeBrowSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeBrowSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeBrowSizeWidthQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeBrowSizeWidthQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryLeftEyeSizeHeight", enabled = true/*Optional*/)

    private void queryLeftEyeSizeHeight(FaceDetails faceDetails){

        Query personLeftEyeSizeHeightQueryStartAt = myRef.orderByChild("mCalculateLeftEyeSizeHeight")
                .startAt(faceDetails.getmCalculateLeftEyeSizeHeight())
                .limitToFirst(1);
        personLeftEyeSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeSizeHeightQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeHeightQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeHeightQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeHeightQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeSizeHeightQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personLeftEyeSizeHeightQueryEndAt = myRef.orderByChild("mCalculateLeftEyeSizeHeight")
                .endAt(faceDetails.getmCalculateLeftEyeSizeHeight())
                .limitToLast(1);
        personLeftEyeSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeHeightQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryLeftEyeSizeWidth", enabled = true/*Optional*/)

    private void queryLeftEyeSizeWidth(FaceDetails faceDetails){

        Query personLeftEyeSizeWidthQueryStartAt = myRef.orderByChild("mCalculateLeftEyeSizeWidth")
                .startAt(faceDetails.getmCalculateLeftEyeSizeWidth())
                .limitToFirst(1);
        personLeftEyeSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personLeftEyeSizeWidthQueryEndAt = myRef.orderByChild("mCalculateLeftEyeSizeWidth")
                .endAt(faceDetails.getmCalculateLeftEyeSizeWidth())
                .limitToLast(1);
        personLeftEyeSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryLeftEyeSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personLeftEyeSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryMouthSizeHeight", enabled = true/*Optional*/)

    private void queryMouthSizeHeight(FaceDetails faceDetails){

        Query personMouthSizeHeightQueryStartAt = myRef.orderByChild("mCalculateMouthSizeHeight")
                .startAt(faceDetails.getmCalculateMouthSizeHeight())
                .limitToFirst(1);
        personMouthSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personMouthSizeHeightQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryMouthSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeHeightQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeHeightQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeHeightQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personMouthSizeHeightQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personMouthSizeHeightQueryEndAt = myRef.orderByChild("mCalculateMouthSizeHeight")
                .endAt(faceDetails.getmCalculateMouthSizeHeight())
                .limitToLast(1);
        personMouthSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personMouthSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryMouthSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeHeightQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personMouthSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryMouthSizeWidth", enabled = true/*Optional*/)

    private void queryMouthSizeWidth(FaceDetails faceDetails){

        Query personMouthSizeWidthQueryStartAt = myRef.orderByChild("mCalculateMouthSizeWidth")
                .startAt(faceDetails.getmCalculateMouthSizeWidth())
                .limitToFirst(1);
        personMouthSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personMouthSizeWidthQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryMouthSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personMouthSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personMouthSizeWidthQueryEndAt = myRef.orderByChild("mCalculateMouthSizeWidth")
                .endAt(faceDetails.getmCalculateMouthSizeWidth())
                .limitToLast(1);
        personMouthSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personMouthSizeWidthQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryMouthSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeWidthQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeWidthQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personMouthSizeWidthQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personMouthSizeWidthQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryNoseSizeHeight", enabled = true/*Optional*/)

    private void queryNoseSizeHeight(FaceDetails faceDetails){

        Query personNoseSizeHeightQueryStartAt = myRef.orderByChild("mCalculateNoseSizeHeight")
                .startAt(faceDetails.getmCalculateNoseSizeHeight())
                .limitToFirst(1);
        personNoseSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personNoseSizeHeightQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryNoseSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeHeightQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeHeightQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeHeightQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personNoseSizeHeightQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personNoseSizeHeightQueryEndAt = myRef.orderByChild("mCalculateNoseSizeHeight")
                .endAt(faceDetails.getmCalculateNoseSizeHeight())
                .limitToLast(1);
        personNoseSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personNoseSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryNoseSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeHeightQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personNoseSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryNoseSizeWidth", enabled = true/*Optional*/)

    private void queryNoseSizeWidth(FaceDetails faceDetails){

        Query personNoseSizeWidthQueryStartAt = myRef.orderByChild("mCalculateNoseSizeWidth")
                .startAt(faceDetails.getmCalculateNoseSizeWidth())
                .limitToFirst(1);
        personNoseSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personNoseSizeWidthQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryNoseSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personNoseSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personNoseSizeWidthQueryEndAt = myRef.orderByChild("mCalculateNoseSizeWidth")
                .endAt(faceDetails.getmCalculateNoseSizeWidth())
                .limitToLast(1);
        personNoseSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personNoseSizeWidthQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryNoseSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeWidthQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeWidthQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personNoseSizeWidthQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personNoseSizeWidthQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryRightEyeBrowSizeHeight", enabled = true/*Optional*/)

    private void queryRightEyeBrowSizeHeight(FaceDetails faceDetails){

        Query personRightEyeBrowSizeHeightQueryStartAt = myRef.orderByChild("mCalculateRightEyeBrowSizeHeight")
                .startAt(faceDetails.getmCalculateRightEyeBrowSizeHeight())
                .limitToFirst(1);
        personRightEyeBrowSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "queryRightEyeBrowSizeHeight:onChildAdded:" + dataSnapshot.getKey());
                    if( checkIfPersonAlreadyFoundInQueryRightEyeBrowSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryRightEyeBrowSizeHeight:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryRightEyeBrowSizeHeight:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryRightEyeBrowSizeHeight:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "queryRightEyeBrowSizeHeight:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personRightEyeBrowSizeHeightQueryEndAt = myRef.orderByChild("mCalculateRightEyeBrowSizeHeight")
                .endAt(faceDetails.getmCalculateRightEyeBrowSizeHeight())
                .limitToLast(1);
        personRightEyeBrowSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personRightEyeBrowSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryRightEyeBrowSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeBrowSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeBrowSizeHeightQueryEndAt:LonChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeBrowSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personRightEyeBrowSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryRightEyeBrowSizeWidth", enabled = true/*Optional*/)

    private void queryRightEyeBrowSizeWidth(FaceDetails faceDetails){

        Query personRightEyeBrowSizeWidthQueryStartAt = myRef.orderByChild("mCalculateRightEyeBrowSizeWidth")
                .startAt(faceDetails.getmCalculateRightEyeBrowSizeWidth())
                .limitToFirst(1);
        personRightEyeBrowSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "queryRightEyeBrowSizeWidth:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryRightEyeBrowSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryRightEyeBrowSizeWidth:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryRightEyeBrowSizeWidth:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "queryRightEyeBrowSizeWidth:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "queryRightEyeBrowSizeWidth:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personRightEyeBrowSizeWidthQueryEndAt = myRef.orderByChild("mCalculateRightEyeBrowSizeWidth")
                .endAt(faceDetails.getmCalculateRightEyeBrowSizeWidth())
                .limitToLast(1);
        personRightEyeBrowSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personRightEyeBrowSizeWidthQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryRightEyeBrowSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeBrowSizeWidthQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeBrowSizeWidthQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeBrowSizeWidthQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personRightEyeBrowSizeWidthQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryRightEyeSizeWidth", enabled = true/*Optional*/)

    private void queryRightEyeSizeWidth(FaceDetails faceDetails){

        Query personRightEyeSizeWidthQueryStartAt = myRef.orderByChild("mCalculateRightEyeSizeWidth")
                .startAt(faceDetails.getmCalculateRightEyeSizeWidth())
                .limitToFirst(1);
        personRightEyeSizeWidthQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personRightEyeSizeWidthQueryStartAt:onChildAdded:" + dataSnapshot.getKey());
                    if( checkIfPersonAlreadyFoundInQueryRightEyeSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeWidthQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeWidthQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeWidthQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personRightEyeSizeWidthQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personRightEyeSizeWidthQueryEndAt = myRef.orderByChild("mCalculateRightEyeSizeWidth")
                .endAt(faceDetails.getmCalculateRightEyeSizeWidth())
                .limitToLast(1);
        personRightEyeSizeWidthQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personRightEyeSizeWidthQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryRightEyeSizeWidth(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeWidthQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeWidthQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeWidthQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personRightEyeSizeWidthQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }
    @AddTrace(name = "queryRightEyeSizeHeight", enabled = true/*Optional*/)

    private void queryRightEyeSizeHeight(FaceDetails faceDetails){

        Query personRightEyeSizeHeightQueryStartAt = myRef.orderByChild("mCalculatedRightEyeSizeHeight")
                .startAt(faceDetails.getmCalculatedRightEyeSizeHeight())
                .limitToFirst(1);
        personRightEyeSizeHeightQueryStartAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personRightEyeSizeHeightQueryStartAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryRightEyeSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }



                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeHeightQueryStartAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeHeightQueryStartAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeHeightQueryStartAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personRightEyeSizeHeightQueryStartAt:DatabaseError:" + databaseError.getMessage());

            }
        });



        Query personRightEyeSizeHeightQueryEndAt = myRef.orderByChild("mCalculatedRightEyeSizeHeight")
                .endAt(faceDetails.getmCalculatedRightEyeSizeHeight())
                .limitToLast(1);
        personRightEyeSizeHeightQueryEndAt.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    String personName = dataSnapshot.getKey();
                    Log.d(TAG, "personRightEyeSizeHeightQueryEndAt:onChildAdded:" + personName);
                    if( checkIfPersonAlreadyFoundInQueryRightEyeSizeHeight(personName) == false){
                        Person person  = dataSnapshot.getValue(Person.class);
                        checkNumberOfMatchesAndWriteToTheDisk(personName,person);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeHeightQueryEndAt:onChildChanged:" + dataSnapshot.getKey());

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeHeightQueryEndAt:onChildRemoved:" + dataSnapshot.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()) {
                    Log.d(TAG, "personRightEyeSizeHeightQueryEndAt:onChildMoved:" + dataSnapshot.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "personRightEyeSizeHeightQueryEndAt:DatabaseError:" + databaseError.getMessage());

            }
        });
    }



    private Boolean checkIfPersonAlreadyFoundInQueryFaceSizeHeight(String keyname){

     if(keyListQueryFaceSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryFaceSizeHeight.add(keyname);
            return false;
        }
    }
    private Boolean checkIfPersonAlreadyFoundInQueryFaceSizeWidth(String keyname){

        if(keyListQueryFaceSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryFaceSizeWidth.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryLeftEyeBrowSizeHeight(String keyname){

        if(keyListQueryLeftEyeBrowSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryLeftEyeBrowSizeHeight.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryLeftEyeBrowSizeWidth(String keyname){

        if(keyListQueryLeftEyeBrowSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryLeftEyeBrowSizeWidth.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryLeftEyeSizeHeight(String keyname){

        if(keyListQueryLeftEyeSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryLeftEyeSizeHeight.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryLeftEyeSizeWidth(String keyname){

        if(keyListQueryLeftEyeSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryLeftEyeSizeWidth.add(keyname);
            return false;
        }
    }
    private Boolean checkIfPersonAlreadyFoundInQueryMouthSizeHeight(String keyname){

        if(keyListQueryMouthSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryMouthSizeHeight.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryMouthSizeWidth(String keyname){

        if(keyListQueryMouthSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryMouthSizeWidth.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryNoseSizeHeight(String keyname){

        if(keyListQueryNoseSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryNoseSizeHeight.add(keyname);
            return false;
        }
    }
    private Boolean checkIfPersonAlreadyFoundInQueryNoseSizeWidth(String keyname){

        if(keyListQueryNoseSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryNoseSizeWidth.add(keyname);
            return false;
        }
    }
    private Boolean checkIfPersonAlreadyFoundInQueryRightEyeBrowSizeHeight(String keyname){

        if(keyListQueryRightEyeBrowSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryRightEyeBrowSizeHeight.add(keyname);
            return false;
        }
    }
    private Boolean checkIfPersonAlreadyFoundInQueryRightEyeBrowSizeWidth(String keyname){

        if(keyListQueryRightEyeBrowSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryRightEyeBrowSizeWidth.add(keyname);
            return false;
        }
    }

    private Boolean checkIfPersonAlreadyFoundInQueryRightEyeSizeWidth(String keyname){

        if(keyListQueryRightEyeSizeWidth.contains(keyname)){
            return true;
        }
        else{
            keyListQueryRightEyeSizeWidth.add(keyname);
            return false;
        }
    }
    private Boolean checkIfPersonAlreadyFoundInQueryRightEyeSizeHeight(String keyname){

        if(keyListQueryRightEyeSizeHeight.contains(keyname)){
            return true;
        }
        else{
            keyListQueryRightEyeSizeHeight.add(keyname);
            return false;
        }
    }



    private void checkNumberOfMatchesAndWriteToTheDisk(String personName,Person person){
        if(listOfMatches.isEmpty()){
            listOfMatches.add(personName);

        }
        else{
                  if(listOfMatches.get(matchesIndex).equals(personName)){
                    listOfMatches.add(personName);
                    matchesIndex++;
                    if(listOfMatches.size() % 14 == 0){
                        Log.d(TAG,"The Query PerfectMatch:"+ personName);
                        Gson gson = new Gson();
                        String response =  gson.toJson(person);

                        mCreateAndSaveFile(response);
                        listOfMatches.clear();
                        matchesIndex = 0;
                        time = 1;
                    }
            }


            }
        }





    public void mCreateAndSaveFile(String mJsonResponse) {
        try {
            Log.d(TAG,"The write response is:" + mJsonResponse);
            FileWriter file = new FileWriter((activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                    + File.separator + "people.json"));
            file.write(mJsonResponse);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String mReadJsonData() {
        String mResponse = "";
        try {

                File file = new File((activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                        + File.separator + "people.json"));
                if(file.exists()){
                    FileInputStream is = new FileInputStream(file);
                    int size = is.available();
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();
                    mResponse = new String(buffer);
                }
                else{
                    Log.d(TAG,"The file does not exist: " + file);
                }
            } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG,"The read response is: " + mResponse);
        return mResponse;
    }

    /**
     *
     * @param for_milli_second
     */
    private void timer(int for_milli_second){
        long start = System.currentTimeMillis();
        long end = start + for_milli_second;
        for (int j = 0; ; j++) {
            if(System.currentTimeMillis() > end)
            {
                break;
            }
        }
    }
}
