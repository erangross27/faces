package com.example.erang.facerecognition.ObjectClass;

import android.graphics.Point;

import java.util.ArrayList;

/**
 * Created by erang on 25-Jun-17.
 */

public class FaceLandmarks {
    public ArrayList<Point>landmarks = new ArrayList<>();

    public FaceLandmarks(ArrayList<Point>landmarks){
        this.landmarks = landmarks;

    }

    public ArrayList<Point> getLandmarks(){
        return landmarks;
    }
}
