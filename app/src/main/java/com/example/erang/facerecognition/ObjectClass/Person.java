package com.example.erang.facerecognition.ObjectClass;





import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by erang on 12-Jul-17.
 */
public class Person implements Serializable {

    private int mData;
    public String name;
    public int age;
    public String id;
   public Map<String, Object> children = new HashMap<String, Object>();
    public String address;
    public String image;
    public Map<String, Object> faceDetails = new HashMap<String, Object>();

    public Person() {
        // Default constructor required for calls to DataSnapshot.getValue(Person.class)
    }


    public Person(String name,int age,String id,Map<String, Object> children,String address,String image,Map<String, Object> faceDetails){

        this.name = name;
        this.age = age;
        this.id = id;
        this.children = children;
        this.address = address;
        this.image = image;
        this.faceDetails = faceDetails;

    }

    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }

    public String getID(){
        return id;
    }
    public Map<String, Object> getChildren(){
        return children;
    }
    public String getAddress(){
        return address;
    }
    public String getImage(){
        return image;
    }
    public Map<String, Object> getFaceDetails(){
        return faceDetails;
    }

    public int describeContents() {
        return 0;
    }



}
