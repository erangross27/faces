package com.example.erang.facerecognition.ObjectClass;

/**
 * Created by erang on 13-Jul-17.
 */

public class FaceDetails {

    public int mCalculateFaceSizeHeight;
    public int mCalculateFaceSizeWidth;
    public int mCalculateLeftEyeBrowSizeHeight;
    public int mCalculateLeftEyeBrowSizeWidth;
    public int mCalculateLeftEyeSizeHeight;
    public int mCalculateLeftEyeSizeWidth;
    public int mCalculateMouthSizeHeight;
    public int mCalculateMouthSizeWidth;
    public int mCalculateNoseSizeHeight;
    public int mCalculateNoseSizeWidth;
    public int mCalculateRightEyeBrowSizeHeight;
    public int mCalculateRightEyeBrowSizeWidth;
    public int mCalculateRightEyeSizeWidth;
    public int mCalculatedRightEyeSizeHeight;

public FaceDetails(int mCalculateFaceSizeHeight,int mCalculateFaceSizeWidth
,int mCalculateLeftEyeBrowSizeHeight, int mCalculateLeftEyeBrowSizeWidth,
                   int mCalculateLeftEyeSizeHeight,int mCalculateLeftEyeSizeWidth,
                   int mCalculateMouthSizeHeight, int mCalculateMouthSizeWidth,
                   int mCalculateNoseSizeHeight, int mCalculateNoseSizeWidth,
                   int mCalculateRightEyeBrowSizeHeight,int mCalculateRightEyeBrowSizeWidth,
                   int mCalculateRightEyeSizeWidth , int mCalculatedRightEyeSizeHeight ){

    this.mCalculateFaceSizeHeight = mCalculateFaceSizeHeight;
    this.mCalculateFaceSizeWidth = mCalculateFaceSizeWidth;
    this.mCalculateLeftEyeBrowSizeHeight = mCalculateLeftEyeBrowSizeHeight;
    this.mCalculateLeftEyeBrowSizeWidth = mCalculateLeftEyeBrowSizeWidth;
    this.mCalculateLeftEyeSizeHeight = mCalculateLeftEyeSizeHeight;
    this.mCalculateLeftEyeSizeWidth = mCalculateLeftEyeSizeWidth;
    this.mCalculateMouthSizeHeight = mCalculateMouthSizeHeight;
    this.mCalculateMouthSizeWidth = mCalculateMouthSizeWidth;
    this.mCalculateNoseSizeHeight = mCalculateNoseSizeHeight;
    this.mCalculateNoseSizeWidth = mCalculateNoseSizeWidth;
    this.mCalculateRightEyeBrowSizeHeight = mCalculateRightEyeBrowSizeHeight;
    this.mCalculateRightEyeBrowSizeWidth = mCalculateRightEyeBrowSizeWidth;
    this.mCalculateRightEyeSizeWidth = mCalculateRightEyeSizeWidth;
    this.mCalculatedRightEyeSizeHeight = mCalculatedRightEyeSizeHeight;


}

public int getmCalculateFaceSizeHeight(){
    return mCalculateFaceSizeHeight;

}
public int getmCalculateFaceSizeWidth(){
    return mCalculateFaceSizeWidth;
}
public int getmCalculateLeftEyeBrowSizeHeight(){
    return mCalculateLeftEyeBrowSizeHeight;
}
public int getmCalculateLeftEyeBrowSizeWidth(){
    return mCalculateLeftEyeBrowSizeWidth;
}
public int getmCalculateLeftEyeSizeHeight(){
    return mCalculateLeftEyeSizeHeight;
}

public int getmCalculateLeftEyeSizeWidth(){
    return mCalculateLeftEyeSizeWidth;
}
public int getmCalculateMouthSizeHeight(){
    return mCalculateMouthSizeHeight;
}
public int getmCalculateMouthSizeWidth(){
    return mCalculateMouthSizeWidth;
}
public int getmCalculateNoseSizeHeight(){
    return mCalculateNoseSizeHeight;
}
public int getmCalculateNoseSizeWidth(){
    return mCalculateNoseSizeWidth;
}
public int getmCalculateRightEyeBrowSizeHeight(){
    return mCalculateRightEyeBrowSizeHeight;
}
public int getmCalculateRightEyeBrowSizeWidth(){
    return mCalculateRightEyeBrowSizeWidth;
}
public int getmCalculateRightEyeSizeWidth(){
    return mCalculateRightEyeSizeWidth;
}
public int getmCalculatedRightEyeSizeHeight(){
    return mCalculatedRightEyeSizeHeight;
}

}

