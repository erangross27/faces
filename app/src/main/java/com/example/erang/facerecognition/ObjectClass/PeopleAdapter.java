package com.example.erang.facerecognition.ObjectClass;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.erang.facerecognition.Activities.DetailResultsActivity;
import com.example.erang.facerecognition.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * Created by erang on 25-May-17.
 */

public class PeopleAdapter extends ArrayAdapter<Person> {

    /**
     * This is our own custom constructor (it doesn't mirror a superclass constructor).
     * The context is used to inflate the layout file, and the list is the data we want
     * to populate into the lists.
     *
     * @param context        The current context. Used to inflate the layout file.
     * @param personArrayList A List of person objects to display in a list
     *

     */
    public PeopleAdapter(Activity context, ArrayList<Person> personArrayList) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.

        super(context, 0, personArrayList);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;


        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }



        // Get the {@link currentEarthQuake} object located at this position in the list

       final  Person currentPerson = getItem(position);
          // Find the TextView in the list_item.xml layout with the ID version_number
        //  Get the Default Magnitude from the currentEarthQuake object and
        final TextView name = (TextView) listItemView.findViewById(R.id.name);

        //set this text on the Person name TextView
        name.setText(currentPerson.getName());

        final ImageView imageView = (ImageView) listItemView.findViewById(R.id.image);

        // Create a storage reference from our app
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a reference to a file from a Google Cloud Storage URI
        StorageReference gsReference = storage.getReferenceFromUrl("gs://facerecognition-29c9e.appspot.com/IMG_20170403_112227_641.jpg");
        String uniquePathName = currentPerson.getName().replaceAll("\\s+","");

        final String mTargetPath = (getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                + File.separator + uniquePathName +".jpg");

        File file = new File(mTargetPath);
        if(! file.exists()) {
            gsReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(mTargetPath);
                    imageView.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "Error occured while download image file" + e.getLocalizedMessage());
                }
            });

        }
        else{
            Bitmap bitmap = BitmapFactory.decodeFile(mTargetPath);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
            }

        }


        //Define a linearLayout so the user can click on it for browser
        LinearLayout firstLinearLayout = (LinearLayout) listItemView.findViewById(R.id.main_layout);

        // Set a click listener on Primary location TextView
        firstLinearLayout.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the linear View is clicked on.
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetailResultsActivity.class);
                intent.putExtra("personDetails",currentPerson);
                getContext().startActivity(intent);

            }
        });



        // so that it can be shown in the ListView
        return listItemView;
    }


   }



