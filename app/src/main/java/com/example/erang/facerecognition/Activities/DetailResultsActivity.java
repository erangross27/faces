package com.example.erang.facerecognition.Activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.erang.facerecognition.ObjectClass.Person;
import com.example.erang.facerecognition.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_results);

        Intent intent = getIntent();
        Person person = (Person) intent.getSerializableExtra("personDetails");
        Locale locale = Locale.getDefault();
        if(person !=null){

        //Setting the name
        TextView fullNameValueTextView = (TextView) findViewById(R.id.fullnameValue);
        fullNameValueTextView.setText(person.getName());

        //Setting the address
            TextView fullAddressValueTextView = (TextView) findViewById(R.id.fulladdressValue);
            fullAddressValueTextView.setText(person.getAddress());

         //Setting the Date of birth
            TextView fullDateOfBirthtextView = (TextView) findViewById(R.id.dateOfBirthValue);
            fullDateOfBirthtextView.setText(String.valueOf(person.getAge()));

            //Setting Identification number
            TextView fullIdentificationNumberTextView = (TextView) findViewById(R.id.identificationValue);
            fullIdentificationNumberTextView.setText(person.getID());

            //Setting Children names

            List<String> keyList = new ArrayList<>(person.getChildren().keySet());
            LinearLayout childrenNameLayout =  (LinearLayout) findViewById(R.id.childrennamelayout);
            LinearLayout.LayoutParams childrenNameLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            childrenNameLayoutParams.setMargins(calculateDp(16), calculateDp(16), calculateDp(16), 0);


            for(int keyListIndex = 0 ; keyListIndex< keyList.size();keyListIndex++){
                TextView newChildName = new TextView(getApplicationContext());
                newChildName.setId(keyListIndex);
                newChildName.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
                newChildName.setTextDirection(View.TEXT_DIRECTION_LOCALE);
                newChildName.setLayoutParams(childrenNameLayoutParams);
                newChildName.setText(keyList.get(keyListIndex));
                newChildName.setTextColor(getColor(R.color.black));
                newChildName.setTextSize(18);
                newChildName.setTypeface(Typeface.DEFAULT_BOLD);
                childrenNameLayout.addView(newChildName);
            }
            //Setting children identification numbers
            List<Object> valueList = new ArrayList<>(person.getChildren().values());
            LinearLayout childrenIdentificationLayout =  (LinearLayout) findViewById(R.id.childrenidentificationnumber);
            LinearLayout.LayoutParams childrenIdentificationLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            childrenIdentificationLayoutParams.setMargins(calculateDp(16), calculateDp(16), calculateDp(16), 0);
            String tempString ="";
            for(int valuesListIndex = 0 ; valuesListIndex< valueList.size();valuesListIndex++){
                TextView newChildIdentification = new TextView(getApplicationContext());
                newChildIdentification.setId(valuesListIndex);
                newChildIdentification.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
                newChildIdentification.setTextDirection(View.TEXT_DIRECTION_LOCALE);
                newChildIdentification.setLayoutParams(childrenNameLayoutParams);

                //Replacing the ID for hebrew translate of ID
                if (locale.getLanguage().equals(new Locale("iw").getLanguage())){
                     tempString = String.valueOf(valueList.get(valuesListIndex)).replaceAll("^\\{(\\S+)\\=(\\S+)\\}", getString(R.string.identification) + " $2");

                }
                else{
                     tempString = String.valueOf(valueList.get(valuesListIndex)).replaceAll("^\\{(\\S+)\\=(\\S+)\\}",getString(R.string.identification) + " $2");
                }


                newChildIdentification.setText(tempString);
                newChildIdentification.setTextColor(getColor(R.color.black));
                newChildIdentification.setTextSize(18);
                newChildIdentification.setTypeface(Typeface.DEFAULT_BOLD);
                childrenIdentificationLayout.addView(newChildIdentification);
            }



        }
    }

    //This function takes DP and return pixel size for layout params
    private int calculateDp(int dpMeasure){
        Resources r = getApplicationContext().getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dpMeasure,
                r.getDisplayMetrics()
        );
        return px;
    }
}
