package com.example.erang.facerecognition.Tasks;

import android.app.Activity;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;

import com.example.erang.facerecognition.ObjectClass.FaceDetails;
import com.example.erang.facerecognition.ObjectClass.FaceLandmarks;

import java.util.ArrayList;

/**
 * Created by erang on 13-Jul-17.
 */

public class CalculateFaceSizeTask extends AsyncTask <ArrayList<ArrayList<FaceLandmarks>>,Void,ArrayList<FaceDetails>>{

    private Activity activity;

    public CalculateFaceSizeTask ( Activity activity){

        this.activity = activity;

    }
    @Override
    protected ArrayList<FaceDetails> doInBackground(ArrayList<ArrayList<FaceLandmarks>>[] arrayLists) {
        ArrayList<FaceDetails> faceDetailsOfMultipleImagesWithMultipleFacesArrayList = new ArrayList<>();
        ArrayList<ArrayList<FaceLandmarks>> multipleImagesFacesLandmarks = arrayLists[0];
            for(int mulipleImagesIndex = 0;mulipleImagesIndex<multipleImagesFacesLandmarks.size();mulipleImagesIndex++ ){
                ArrayList <FaceLandmarks> singleImageFaceLandmarks = multipleImagesFacesLandmarks.get(mulipleImagesIndex);
                for(int singleImageIndex = 0;singleImageIndex<singleImageFaceLandmarks.size();singleImageIndex++){
                    ArrayList<Point> landmarks  = singleImageFaceLandmarks.get(singleImageIndex).getLandmarks();
                        for(int index = 0; index<landmarks.size();index++){
                        if(landmarks.get(index).x == 0){
                            Log.d("Found X at landmarks:",String.valueOf(index));
                            Point point = new Point();
                            point.set(1,landmarks.get(index).y);
                            landmarks.set(index,point);
                        }
                        if(landmarks.get(index).y == 0){
                            Log.d("Found Y at landmarks:",String.valueOf(index));
                            Point point = new Point();
                            point.set(landmarks.get(index).x,1);
                            landmarks.set(index,point);
                        }
                    }

                    faceDetailsOfMultipleImagesWithMultipleFacesArrayList.add(entireCalculation(landmarks));


                }
            }

        return faceDetailsOfMultipleImagesWithMultipleFacesArrayList;
    }





@Override
protected void onPostExecute(ArrayList<FaceDetails> results){
        QueryFaceDetailsTask queryFaceDetailsTask = new QueryFaceDetailsTask(activity);
        queryFaceDetailsTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR,results);


}

private FaceDetails entireCalculation(ArrayList<Point> singleFaceLandmarks){
    //Caluculating the entire face
    int mCalculateFaceSizeHeightValue = mCalculateFaceSizeHeight(singleFaceLandmarks);
    int mCalculateFaceSizeWidthValue = mCalculateFaceSizeWidth(singleFaceLandmarks);
    int mCalculateLeftEyeBrowSizeHeightValue = mCalculateLeftEyeBrowSizeHeight(singleFaceLandmarks);
    int mCalculateLeftEyeBrowSizeWidthValue = mCalculateLeftEyeBrowSizeWidth(singleFaceLandmarks);
    int mCalculateLeftEyeSizeHeightValue = mCalculateLeftEyeSizeHeight(singleFaceLandmarks);
    int mCalculateLeftEyeSizeWidthValue = mCalculateLeftEyeSizeWidth(singleFaceLandmarks);
    int mCalculateMouthSizeHeightValue = mCalculateMouthSizeHeight(singleFaceLandmarks);
    int mCalculateMouthSizeWidthValue =   mCalculateMouthSizeWidth(singleFaceLandmarks);
    int mCalculateNoseSizeHeightValue = mCalculateNoseSizeHeight(singleFaceLandmarks);
    int mCalculateNoseSizeWidthValue = mCalculateNoseSizeWidth(singleFaceLandmarks);
    int mCalculateRightEyeBrowSizeHeightValue = mCalculateRightEyeBrowSizeHeight(singleFaceLandmarks);
    int mCalculateRightEyeBrowSizeWidthValue = mCalculateRightEyeBrowSizeWidth(singleFaceLandmarks);
    int mCalculateRightEyeSizeWidthValue = mCalculateRightEyeSizeWidth(singleFaceLandmarks);
    int mCalculatedRightEyeSizeHeightValue = mCalculatedRightEyeSizeHeight(singleFaceLandmarks);

        //Creating face details object with all face details
       FaceDetails faceDetails =  new FaceDetails(mCalculateFaceSizeHeightValue,
            mCalculateFaceSizeWidthValue,mCalculateLeftEyeBrowSizeHeightValue,mCalculateLeftEyeBrowSizeWidthValue
            ,mCalculateLeftEyeSizeHeightValue,mCalculateLeftEyeSizeWidthValue,mCalculateMouthSizeHeightValue,
            mCalculateMouthSizeWidthValue,mCalculateNoseSizeHeightValue,mCalculateNoseSizeWidthValue,
            mCalculateRightEyeBrowSizeHeightValue,mCalculateRightEyeBrowSizeWidthValue,
            mCalculateRightEyeSizeWidthValue,mCalculatedRightEyeSizeHeightValue);
            return faceDetails;
}


        private int distanceCalculation(Point pointOne, Point pointTwo){
            double distance = Math.sqrt(Math.pow((pointOne.x-pointTwo.x), 2) + Math.pow((pointOne.y-pointTwo.y), 2));
            return (int) Math.round(distance);
        }

        private int mCalculateLeftEyeSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceWidthLeftEye = distanceCalculation(singleFaceLandmarks.get(46),singleFaceLandmarks.get(43));
            return mDistanceWidthLeftEye;

        }
        private int mCalculateLeftEyeSizeHeight(ArrayList<Point> singleFaceLandmarks){
            int mDistanceHeightLeftEyePoint1 = distanceCalculation(singleFaceLandmarks.get(48), singleFaceLandmarks.get(44));
            int mDistanceHeightLeftEyePoint2 = distanceCalculation(singleFaceLandmarks.get(47), singleFaceLandmarks.get(45));
            int mDistanceHeightLeftEyePoint = (mDistanceHeightLeftEyePoint1 + mDistanceHeightLeftEyePoint2) / 2 ;
            return mDistanceHeightLeftEyePoint;
        }

        private int mCalculateRightEyeSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceWidthRightEye = distanceCalculation(singleFaceLandmarks.get(37),singleFaceLandmarks.get(40));
            return mDistanceWidthRightEye;
        }

        private int mCalculatedRightEyeSizeHeight(ArrayList<Point> singleFaceLandmarks){
            int mDistanceHeightRightEyePoint1 = distanceCalculation(singleFaceLandmarks.get(38), singleFaceLandmarks.get(42));
            int mDistanceHeightRightEyePoint2 = distanceCalculation(singleFaceLandmarks.get(39), singleFaceLandmarks.get(41));
            int mDistanceHeightRightEyePoint = (mDistanceHeightRightEyePoint1 + mDistanceHeightRightEyePoint2) / 2 ;
            return mDistanceHeightRightEyePoint;

        }
        private int mCalculateNoseSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceNoseWidth = distanceCalculation(singleFaceLandmarks.get(32),singleFaceLandmarks.get(36));
            return mDistanceNoseWidth;
        }
        private int mCalculateNoseSizeHeight(ArrayList<Point> singleFaceLandmarks){
            int mDistanceNoseHeight = distanceCalculation(singleFaceLandmarks.get(28),singleFaceLandmarks.get(34));
            return mDistanceNoseHeight;
        }

        private int mCalculateMouthSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceMouthWidthPoint1 = distanceCalculation(singleFaceLandmarks.get(49),singleFaceLandmarks.get(55));
            int mDistanceMouthWidthPoint2 = distanceCalculation(singleFaceLandmarks.get(50),singleFaceLandmarks.get(54));
            int mDistanceMouthWidthPoint3 = distanceCalculation(singleFaceLandmarks.get(60),singleFaceLandmarks.get(56));
            int mDistanceMouthWidthPoint4 = distanceCalculation(singleFaceLandmarks.get(59),singleFaceLandmarks.get(57));
            int mDistanceMouthWidthPoint5 = distanceCalculation(singleFaceLandmarks.get(51),singleFaceLandmarks.get(53));
            int mDistanceMouthWidth = (mDistanceMouthWidthPoint1 + mDistanceMouthWidthPoint2 +
                    mDistanceMouthWidthPoint3 + mDistanceMouthWidthPoint4 + mDistanceMouthWidthPoint5) / 5;

            return mDistanceMouthWidth;
        }

        private int mCalculateMouthSizeHeight(ArrayList<Point> singleFaceLandmarks){
            int mDistanceMouthHeightPoint1 = distanceCalculation(singleFaceLandmarks.get(52),singleFaceLandmarks.get(58));
            int mDistanceMouthHeightPoint2 = distanceCalculation(singleFaceLandmarks.get(51),singleFaceLandmarks.get(59));
            int mDistanceMouthHeightPoint3 = distanceCalculation(singleFaceLandmarks.get(53),singleFaceLandmarks.get(57));
            int mDistanceMouthHeightPoint4 = distanceCalculation(singleFaceLandmarks.get(54),singleFaceLandmarks.get(56));
            int mDistanceMouthHeightPoint5 = distanceCalculation(singleFaceLandmarks.get(50),singleFaceLandmarks.get(60));
            int mDistanceMouthHeight = (mDistanceMouthHeightPoint1 + mDistanceMouthHeightPoint2 +
                    mDistanceMouthHeightPoint3 + mDistanceMouthHeightPoint4 + mDistanceMouthHeightPoint5) /5 ;
            return mDistanceMouthHeight;
        }

        private int mCalculateLeftEyeBrowSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceLeftEyeBrowWidthtPoint1 = distanceCalculation(singleFaceLandmarks.get(25),singleFaceLandmarks.get(23));
            int mDistanceLeftEyeBrowWidthPoint2 = distanceCalculation(singleFaceLandmarks.get(25),singleFaceLandmarks.get(27));
            int mDistanceLeftEyeBrowWidth = (mDistanceLeftEyeBrowWidthtPoint1 + mDistanceLeftEyeBrowWidthPoint2) /2 ;
            return mDistanceLeftEyeBrowWidth;
        }

        private int mCalculateLeftEyeBrowSizeHeight(ArrayList<Point> singleFaceLandmarks){

            int mDistanceLeftEyeBrowHeightPoint1 = distanceCalculation(singleFaceLandmarks.get(23),singleFaceLandmarks.get(27));
            int mDistanceLeftEyeBrowHeightPoint2 = distanceCalculation(singleFaceLandmarks.get(24),singleFaceLandmarks.get(26));
            int mDistanceLeftEyeBrowHeight = (mDistanceLeftEyeBrowHeightPoint1 + mDistanceLeftEyeBrowHeightPoint2) / 2;
            return mDistanceLeftEyeBrowHeight;
        }

        private int mCalculateRightEyeBrowSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceRightEyeBrowWidthPoint1 = distanceCalculation(singleFaceLandmarks.get(18),singleFaceLandmarks.get(22));
            int mDistanceRightEyeBrowWidthPoint2 = distanceCalculation(singleFaceLandmarks.get(19),singleFaceLandmarks.get(21));
            int mDistanceRightEyeBrowWidth = (mDistanceRightEyeBrowWidthPoint1 + mDistanceRightEyeBrowWidthPoint2) /2 ;
            return mDistanceRightEyeBrowWidth;

        }

        private int mCalculateRightEyeBrowSizeHeight(ArrayList<Point> singleFaceLandmarks){
            int mDistanceRightEyeBrowHeightPoint1 = distanceCalculation(singleFaceLandmarks.get(20),singleFaceLandmarks.get(28));
            int mDistanceRightEyeBrowHeightPoint2 = distanceCalculation(singleFaceLandmarks.get(20),singleFaceLandmarks.get(22));
            int mDistanceRightEyeBrow = (mDistanceRightEyeBrowHeightPoint1 + mDistanceRightEyeBrowHeightPoint2) / 2;
            return mDistanceRightEyeBrow;
        }

        private int mCalculateFaceSizeWidth(ArrayList<Point> singleFaceLandmarks){
            int mDistanceFaceWidthPoint1 = distanceCalculation(singleFaceLandmarks.get(1),singleFaceLandmarks.get(17));
            int mDistanceFaceWidthPoint2 = distanceCalculation(singleFaceLandmarks.get(2),singleFaceLandmarks.get(16));
            int mDistanceFaceWidthPoint3 = distanceCalculation(singleFaceLandmarks.get(3),singleFaceLandmarks.get(15));
            int mDistanceFaceWidthPoint4 = distanceCalculation(singleFaceLandmarks.get(4),singleFaceLandmarks.get(14));
            int mDistanceFaceWidthPoint5 = distanceCalculation(singleFaceLandmarks.get(5),singleFaceLandmarks.get(13));
            int mDistanceFaceWidthPoint6 = distanceCalculation(singleFaceLandmarks.get(6),singleFaceLandmarks.get(12));
            int mDistanceFaceWidthPoint7 = distanceCalculation(singleFaceLandmarks.get(7),singleFaceLandmarks.get(11));
            int mDistanceFaceWidthPoint8 = distanceCalculation(singleFaceLandmarks.get(8),singleFaceLandmarks.get(10));
            int mDistanceFaceWidth = (mDistanceFaceWidthPoint1 + mDistanceFaceWidthPoint2 +
                    mDistanceFaceWidthPoint3 + mDistanceFaceWidthPoint4 +mDistanceFaceWidthPoint5 +
                    mDistanceFaceWidthPoint6 + mDistanceFaceWidthPoint7 + mDistanceFaceWidthPoint8) / 8;
            //Return Face width average size
            return mDistanceFaceWidth;
        }

        private int mCalculateFaceSizeHeight(ArrayList<Point> singleFaceLandmarks){
            int mDistanceFaceHeightPoint1 = distanceCalculation(singleFaceLandmarks.get(18),singleFaceLandmarks.get(5));
            int mDistanceFaceHeightPoint2 = distanceCalculation(singleFaceLandmarks.get(19),singleFaceLandmarks.get(6));
            int mDistanceFaceHeightPoint3 = distanceCalculation(singleFaceLandmarks.get(20),singleFaceLandmarks.get(7));
            int mDistanceFaceHeightPoint4 = distanceCalculation(singleFaceLandmarks.get(40),singleFaceLandmarks.get(8));
            int mDistanceFaceHeightPoint5 = distanceCalculation(singleFaceLandmarks.get(28),singleFaceLandmarks.get(9));
            int mDistanceFaceHeightPoint6 = distanceCalculation(singleFaceLandmarks.get(43),singleFaceLandmarks.get(10));
            int mDistanceFaceHeightPoint7 = distanceCalculation(singleFaceLandmarks.get(25),singleFaceLandmarks.get(11));
            int mDistanceFaceHeightPoint8 = distanceCalculation(singleFaceLandmarks.get(26),singleFaceLandmarks.get(12));
            int mDistanceFaceHeightPoint9 = distanceCalculation(singleFaceLandmarks.get(27),singleFaceLandmarks.get(13));
            int mDistanceFaceHeight = (mDistanceFaceHeightPoint1 + mDistanceFaceHeightPoint2 +
                    mDistanceFaceHeightPoint3 + mDistanceFaceHeightPoint4 + mDistanceFaceHeightPoint5 +
                    mDistanceFaceHeightPoint6 + mDistanceFaceHeightPoint7 + mDistanceFaceHeightPoint8 +
                    mDistanceFaceHeightPoint9) / 9;
            return mDistanceFaceHeight;
        }



}



