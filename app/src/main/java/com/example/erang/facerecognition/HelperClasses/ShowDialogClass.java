package com.example.erang.facerecognition.HelperClasses;

import android.app.Activity;
import android.app.ProgressDialog;

import com.example.erang.facerecognition.R;

/**
 * Created by erang on 19-Jul-17.
 */

public class ShowDialogClass {
    private Activity mActivity;
    public ProgressDialog mProgressDialog;

    public ShowDialogClass(){

    }

    public ShowDialogClass(Activity activity){

        mActivity = activity;


    }

    public void showDialog(){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage(mActivity.getString(R.string.analayzeimage));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();

        }



    }
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}
