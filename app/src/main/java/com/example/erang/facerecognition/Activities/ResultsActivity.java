package com.example.erang.facerecognition.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.erang.facerecognition.ObjectClass.PeopleAdapter;
import com.example.erang.facerecognition.ObjectClass.Person;
import com.example.erang.facerecognition.R;

import java.util.ArrayList;

public class ResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Intent intent = getIntent();
        ArrayList<Person> personArrayList = (ArrayList<Person>) intent.getSerializableExtra("personArrayList");

        PeopleAdapter adapter = new PeopleAdapter(this, personArrayList);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // activity_results.xml layout file.

        ListView listView = (ListView) findViewById(R.id.list);


        // Make the {@link ListView} use the {@link WordAdapter} we created above, so that the
        // {@link ListView} will display list items for each {@link Person} in the list.
        listView.setAdapter(adapter);

    }
}
