package com.example.erang.facerecognition.Tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.example.erang.facerecognition.ObjectClass.FaceLandmarks;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.VisionDetRet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by erang on 13-Jun-17.
 */

public class AnalyzeImageTask extends AsyncTask<ArrayList<String>,Void,ArrayList<ArrayList<FaceLandmarks>>> {


    private String mTargetPath;
    private Activity mActivity;


    public AnalyzeImageTask ( Activity activity){

        mActivity = activity;

    }



    @Override
    protected void onPreExecute() {

        copyRawOnAppFolder();

    }


    @Override
    protected ArrayList<ArrayList<FaceLandmarks>> doInBackground(ArrayList<String>[] list) {

        ArrayList<ArrayList<FaceLandmarks>> multipleImagesFacesLandmarks = new ArrayList<>();
        int count = list.length;

        for (int i = 0; i < count; i++) {
            int mListOfNumberOfImages = list[i].size();
            for(int index = 0; index<mListOfNumberOfImages;index++){
                Bitmap bitmap = BitmapFactory.decodeFile(list[i].get(index));
                if(bitmap != null){
                    multipleImagesFacesLandmarks.add(faceLandmarkDetection(bitmap)) ;
                }
               //After extracting information from image deleting the file.
               File file = new File(list[i].get(index));
               file.delete();
            }

        }


        return multipleImagesFacesLandmarks;
    }


    @Override
    protected void onPostExecute(ArrayList<ArrayList<FaceLandmarks>> results){
        CalculateFaceSizeTask calculateFaceSizeTask = new CalculateFaceSizeTask(mActivity);
        calculateFaceSizeTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR,results);
    }




//This function extract data from the image and return FaceLandmarks Object
private ArrayList<FaceLandmarks> faceLandmarkDetection(Bitmap bitmap) {
     ArrayList<Point> singleFaceLandmarks = new ArrayList<>();
     ArrayList<FaceLandmarks> multipleFacesLandmarks = new ArrayList<>();
    //Calling FaceDet class loading landmarks file.
    FaceDet faceDet = new FaceDet(mTargetPath);
    //This array contain multiple faces landmarks
    //extracting the results from the image
    Bitmap rotateBitmap = rotateImage(bitmap);
    List<VisionDetRet> results = faceDet.detect(rotateBitmap);
    //Running on the image faces extracting information.
    for (VisionDetRet ret : results) {
        int rectLeft = ret.getLeft();
        int rectTop = ret.getTop();
        int rectRight = ret.getRight();
        int rectBottom = ret.getBottom();
        int imageWidth = rotateBitmap.getWidth();
        int imageHeight = rotateBitmap.getHeight();
        //We do image correction, if the face is out of the entire image frame
        if(rectRight > imageWidth){
            rectRight = imageWidth;
        }
        //We do image correction, if the face is out of the entire image frame
        if(rectBottom > imageHeight){
            rectBottom = imageHeight;
        }

        int faceWidth = rectRight - rectLeft;
        int faceHeight = rectBottom - rectTop;

            Bitmap croppedBmp = Bitmap.createBitmap(rotateBitmap, rectLeft, rectTop, faceWidth, faceHeight);
            List<VisionDetRet> improvedResults = faceDet.detect(croppedBmp);
            //This array contain single face landmarks: Get 68 landmark points
            for (VisionDetRet improvedRet : improvedResults) {
                singleFaceLandmarks = improvedRet.getFaceLandmarks();
            }
            multipleFacesLandmarks.add(new FaceLandmarks(singleFaceLandmarks));
        }




    return multipleFacesLandmarks;
}

//Copy the landmarkModel file to the application folder

    private void copyRawOnAppFolder() {

        mTargetPath = (mActivity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                + File.separator + "shape_predictor_68_face_landmarks.dat");
        File file = new File(mTargetPath);
        if (!file.exists()) {
            //Instantiate the ProgressDialog until data is arrive
            final ProgressDialog pd = ProgressDialog.show(mActivity, "Copy Configuration file", "Copy files for the first time Please Wait",
                    true);
            pd.show();
            AssetManager assetManager = (mActivity.getAssets());
            String[] files = null;
            try {
                files = assetManager.list("");
            } catch (IOException e) {
                Log.e("tag", "Failed to get asset file list.", e);
            }
            if (files != null) for (String filename : files) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(filename);
                    File outFile = new File(mTargetPath);
                    out = new FileOutputStream(outFile);
                    copyFile(in, out);
                } catch (IOException e) {
                    Log.e("tag", "Failed to copy asset file: " + filename, e);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                }
            }
            pd.cancel();
        }
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

private Bitmap rotateImage (Bitmap bitmapOrg){
    Matrix matrix = new Matrix();

    matrix.postRotate(270);

    Bitmap rotatedBitmap = Bitmap.createBitmap(bitmapOrg , 0, 0, bitmapOrg .getWidth(), bitmapOrg .getHeight(), matrix, true);

    return rotatedBitmap;
}



}
